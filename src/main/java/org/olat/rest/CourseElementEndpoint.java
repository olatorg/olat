/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.rest;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.olat.restapi.repository.course.AbstractCourseNodeWebService;

/**
 * @author Christian Schweizer
 * @since 5.4
 */
@Path("repo/courses/{courseId}/elements")
public class CourseElementEndpoint extends AbstractCourseNodeWebService {

  @PUT
  @Path("individual-task")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public Response attachIndividualTask(
      @PathParam("courseId") Long courseId,
      @QueryParam("parentNodeId") String parentNodeId,
      @QueryParam("position") Integer position,
      @QueryParam("shortTitle") String shortTitle,
      @QueryParam("longTitle") @DefaultValue("undefined") String longTitle,
      @QueryParam("description") String description,
      @QueryParam("objectives") String objectives,
      @QueryParam("instruction") String instruction,
      @QueryParam("instructionalDesign") String instructionalDesign,
      @QueryParam("visibilityExpertRules") String visibilityExpertRules,
      @QueryParam("accessExpertRules") String accessExpertRules,
      @Context HttpServletRequest request) {
    return attach(
        courseId,
        parentNodeId,
        "ita",
        position,
        shortTitle,
        longTitle,
        description,
        objectives,
        instruction,
        instructionalDesign,
        visibilityExpertRules,
        accessExpertRules,
        null,
        request);
  }

  @PUT
  @Path("group-task")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public Response attachGroupTask(
      @PathParam("courseId") Long courseId,
      @QueryParam("parentNodeId") String parentNodeId,
      @QueryParam("position") Integer position,
      @QueryParam("shortTitle") String shortTitle,
      @QueryParam("longTitle") @DefaultValue("undefined") String longTitle,
      @QueryParam("description") String description,
      @QueryParam("objectives") String objectives,
      @QueryParam("instruction") String instruction,
      @QueryParam("instructionalDesign") String instructionalDesign,
      @QueryParam("visibilityExpertRules") String visibilityExpertRules,
      @QueryParam("accessExpertRules") String accessExpertRules,
      @Context HttpServletRequest request) {
    return attach(
        courseId,
        parentNodeId,
        "gta",
        position,
        shortTitle,
        longTitle,
        description,
        objectives,
        instruction,
        instructionalDesign,
        visibilityExpertRules,
        accessExpertRules,
        null,
        request);
  }
}
