/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.headerfilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Christian Guretzki
 * @since 5.6
 */
@Slf4j
public class RemoveCspHeaderFilter extends HttpFilter {

  private static final String CONTENT_SECURITY_POLICY_REPORT_ONLY =
      "Content-Security-Policy-Report-Only";
  private static final String CONTENT_SECURITY_POLICY = "Content-Security-Policy";

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    super.init(filterConfig);
    log.info(
        "CspHeaderFilter is activ: Header '"
            + CONTENT_SECURITY_POLICY_REPORT_ONLY
            + "' and '"
            + CONTENT_SECURITY_POLICY
            + "' will be filtered. Add CSP-configuration in apache.");
  }

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    log.debug("doFilter");
    filterChain.doFilter(
        servletRequest,
        new HttpServletResponseWrapper((HttpServletResponse) servletResponse) {

          @Override
          public void setHeader(String name, String value) {
            log.debug("setHeader('{}', '{}')", name, value);
            if (!CONTENT_SECURITY_POLICY_REPORT_ONLY.equalsIgnoreCase(name)
                && !CONTENT_SECURITY_POLICY.equalsIgnoreCase(name)) {
              super.setHeader(name, value);
            } else {
              log.debug("Does NOT call setHeader('{}', '{}')", name, value);
            }
          }

          @Override
          public void addHeader(String name, String value) {
            log.debug("addHeader('{}', '{}')", name, value);
            if (!CONTENT_SECURITY_POLICY_REPORT_ONLY.equalsIgnoreCase(name)
                && !CONTENT_SECURITY_POLICY.equalsIgnoreCase(name)) {
              super.setHeader(name, value);
            } else {
              log.debug("Does NOT call addHeader('{}', '{}')", name, value);
            }
          }
        });
  }
}
