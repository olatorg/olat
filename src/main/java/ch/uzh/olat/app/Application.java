/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.app;

import ch.uzh.olat.csp.ui.CspAdminController;
import ch.uzh.olat.headerfilter.RemoveCspHeaderFilter;
import ch.uzh.olat.monitoring.RequestResponseStatisticFilter;
import ch.uzh.olat.monitoring.RequestResponseTimeStatisticCollector;
import ch.uzh.olat.ui.ModuleVersionController;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.extensions.action.GenericActionExtension;
import org.olat.core.gui.control.creator.AutoCreator;
import org.olat.extension.core.commons.services.analytics.spi.MatomoTrackingFeature;
import org.olat.openolat.customizer.WordReplacementLocalStringsCustomizer;
import org.olat.openolat.processor.AfterInitializationBeanProcessor;
import org.olat.rest.CourseElementEndpoint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

/**
 * @author Christian Schweizer
 * @since 4.2
 */
@SpringBootApplication
@ComponentScan({"ch.uzh.olat.csp.service", "ch.uzh.olat.monitoring"})
@Slf4j
public class Application {

  @Value(
      "${monitoring.request.response.statistic.filter.urlPatterns:/auth/MyCoursesSite/*,/auth/RepositoryEntry/*, /auth/GroupsSite/*,/auth/BusinessGroup/*}")
  private String[] urlPatterns;

  @Bean
  public CourseElementEndpoint courseElementEndpoint() {
    return new CourseElementEndpoint();
  }

  @Bean
  public MatomoTrackingFeature disableHeatmapSessionRecording() {
    return features -> features.append("_paq.push(['HeatmapSessionRecording::disable']);\r");
  }

  @Bean
  public WordReplacementLocalStringsCustomizer wordReplacementLocalStringsCustomizer() {
    return new WordReplacementLocalStringsCustomizer(
        Map.of("OpenOlat", "OLAT"),
        Map.of(
            "org.olat.login",
            new String[] {
              "about.elearningjournal.test.2015",
              "about.financing.intro",
              "about.history",
              "about.partner",
              "about.partner.intro"
            }));
  }

  @Bean
  public AfterInitializationBeanProcessor<GenericActionExtension>
      olatOrgParentExtensionCustomizer() {
    return new AfterInitializationBeanProcessor<>(
        "olatParentExtension", GenericActionExtension.class) {

      @Override
      public GenericActionExtension process(GenericActionExtension extension) {
        AutoCreator creator = new AutoCreator();
        creator.setClassName(ModuleVersionController.class.getName());
        extension.setActionController(creator);
        return extension;
      }
    };
  }

  @Profile("production")
  @ConditionalOnExpression("${remove.csp.header.filter.enabled:true}")
  @Bean
  public FilterRegistrationBean<RemoveCspHeaderFilter> removeCspHeaderFilter() {
    FilterRegistrationBean<RemoveCspHeaderFilter> bean = new FilterRegistrationBean<>();
    bean.setName("RemoveCspHeaderFilter");
    bean.setFilter(new RemoveCspHeaderFilter());
    bean.addUrlPatterns("/*");
    bean.setOrder(0);
    return bean;
  }

  @Bean(initMethod = "initExtensionPoints")
  public GenericActionExtension uzhCspAdminExtension() {
    GenericActionExtension extension = new GenericActionExtension();
    List<String> extensionPoints = new ArrayList<>();
    extensionPoints.add("org.olat.admin.SystemAdminMainController");
    extension.setExtensionPoints(extensionPoints);
    AutoCreator controller = new AutoCreator();
    controller.setClassName(CspAdminController.class.getName());
    extension.setActionController(controller);
    extension.setParentTreeNodeIdentifier("uzhParent");
    extension.setNavigationKey("uzhCsp");
    extension.setTranslationPackage("ch.uzh.olat.csp.ui");
    extension.setI18nActionKey("menu.uzh.csp");
    extension.setI18nDescriptionKey("menu.uzh.csp.alt");
    return extension;
  }

  @Bean
  public RequestResponseTimeStatisticCollector requestStatisticCollector() {
    return new RequestResponseTimeStatisticCollector();
  }

  @ConditionalOnExpression("${monitoring.request.response.statistic.filter.enabled:true}")
  @Bean
  public FilterRegistrationBean<RequestResponseStatisticFilter> requestStatisticFilter() {
    FilterRegistrationBean<RequestResponseStatisticFilter> bean = new FilterRegistrationBean<>();
    bean.setName("RequestStatisticFilter");
    bean.setFilter(new RequestResponseStatisticFilter());
    bean.addUrlPatterns(urlPatterns);
    bean.setOrder(0);
    return bean;
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
