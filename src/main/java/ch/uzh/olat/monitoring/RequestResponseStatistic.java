/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.monitoring;

import lombok.extern.slf4j.Slf4j;
import org.olat.core.CoreSpringFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 * @author Christian Guretzki
 * @since 6.1
 */
@Component
@ManagedResource(
    objectName = "ch.uzh.olat:type=basic,name=RequestResponseStatistic",
    description = "Request response statistic")
@Slf4j
public class RequestResponseStatistic implements RequestResponseStatisticMBean {

  private RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector;

  @ManagedAttribute(
      description =
          "Maximale request response time in millis of the last N(default=1000) requests.")
  public long getMaxRequestResponseTime() {
    return getRequestStatisticCollector().getMaxRequestResponseTime();
  }

  @ManagedAttribute(
      description = "Minimal request response time in millis of the last N(default=1000) requests.")
  public long getMinRequestResponseTime() {
    return getRequestStatisticCollector().getMinRequestResponseTime();
  }

  @ManagedAttribute(
      description = "Mean request response time in millis of the last N(default=1000) requests.")
  public double getMeanRequestResponseTime() {
    return getRequestStatisticCollector().getMeanRequestResponseTime();
  }

  private RequestResponseTimeStatisticCollector getRequestStatisticCollector() {
    if (requestResponseTimeStatisticCollector == null) {
      requestResponseTimeStatisticCollector =
          CoreSpringFactory.getImpl(RequestResponseTimeStatisticCollector.class);
    }
    return requestResponseTimeStatisticCollector;
  }
}
