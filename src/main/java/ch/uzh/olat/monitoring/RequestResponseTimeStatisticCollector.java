/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.monitoring;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.TestOnly;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Christian Guretzki
 * @since 6.1
 */
@Slf4j
public class RequestResponseTimeStatisticCollector {

  @Value("${monitoring.max.buffer_elments:50}")
  private int maxElementsInBuffer;

  @Value("${monitoring.max.buffer_timestamp:120000}")
  private int maxTimestampInBuffer;

  private final List<RequestResponseTime> requestResponseTimeList =
      new ArrayList<>(maxElementsInBuffer);

  public RequestResponseTimeStatisticCollector() {}

  public RequestResponseTimeStatisticCollector(int maxElementsInBuffer, int maxTimestampInBuffer) {
    this.maxElementsInBuffer = maxElementsInBuffer;
    this.maxTimestampInBuffer = maxTimestampInBuffer;
  }

  public long getMaxRequestResponseTime() {
    synchronized (requestResponseTimeList) {
      if (hasExpiredElements(requestResponseTimeList)) {
        return getFilteredMaxRequestResponseTime(requestResponseTimeList);
      } else {
        return getMaxRequestResponseTime(requestResponseTimeList);
      }
    }
  }

  @TestOnly
  long getMaxRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    return requestResponseTimeList.stream()
        .max(Comparator.comparingLong(RequestResponseTime::getTimeValue))
        .map(RequestResponseTime::getTimeValue)
        .orElse(0L);
  }

  @TestOnly
  boolean hasExpiredElements(List<RequestResponseTime> requestResponseTimeList) {
    if (!requestResponseTimeList.isEmpty()) {
      return new Date().getTime() - requestResponseTimeList.get(0).getTimeStamp()
          > maxTimestampInBuffer;
    } else {
      return false;
    }
  }

  @TestOnly
  long getFilteredMaxRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    long now = System.currentTimeMillis();
    return requestResponseTimeList.stream()
        .filter(r -> now - r.getTimeStamp() < maxTimestampInBuffer)
        .max(Comparator.comparingLong(RequestResponseTime::getTimeValue))
        .map(RequestResponseTime::getTimeValue)
        .orElse(0L);
  }

  public long getMinRequestResponseTime() {
    synchronized (requestResponseTimeList) {
      if (hasExpiredElements(requestResponseTimeList)) {
        return getFilteredMinRequestResponseTime(requestResponseTimeList);
      } else {
        return getMinRequestResponseTime(requestResponseTimeList);
      }
    }
  }

  @TestOnly
  long getMinRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    return requestResponseTimeList.stream()
        .min(Comparator.comparingLong(RequestResponseTime::getTimeValue))
        .map(RequestResponseTime::getTimeValue)
        .orElse(0L);
  }

  @TestOnly
  long getFilteredMinRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    long now = System.currentTimeMillis();
    return requestResponseTimeList.stream()
        .filter(r -> now - r.getTimeStamp() < maxTimestampInBuffer)
        .min(Comparator.comparingLong(RequestResponseTime::getTimeValue))
        .map(RequestResponseTime::getTimeValue)
        .orElse(0L);
  }

  public double getMeanRequestResponseTime() {
    synchronized (requestResponseTimeList) {
      log.debug(
          "getMeanRequestResponseTime: #requestResponseTimeList={}",
          requestResponseTimeList.size());
      if (hasExpiredElements(requestResponseTimeList)) {
        return getFilteredMeanRequestResponseTime(requestResponseTimeList);
      } else {
        return getMeanRequestResponseTime(requestResponseTimeList);
      }
    }
  }

  @TestOnly
  double getMeanRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    log.debug(
        "getMeanRequestResponseTime (without filter) : #requestResponseTimeList={}",
        requestResponseTimeList.size());
    return requestResponseTimeList.stream()
        .mapToLong(RequestResponseTime::getTimeValue)
        .average()
        .orElse(0);
  }

  @TestOnly
  double getFilteredMeanRequestResponseTime(List<RequestResponseTime> requestResponseTimeList) {
    log.debug("getFilteredMeanRequestResponseTime: #maxTimestampInBuffer={}", maxTimestampInBuffer);
    long now = new Date().getTime();
    return requestResponseTimeList.stream()
        .filter(r -> now - r.getTimeStamp() < maxTimestampInBuffer)
        .mapToLong(RequestResponseTime::getTimeValue)
        .average()
        .orElse(0);
  }

  public void addRequestResponseTime(long requestResponseTime) {
    log.debug("addRequestResponseTime: requestResponseTime={}", requestResponseTime);
    synchronized (requestResponseTimeList) {
      if (requestResponseTimeList.size() >= maxElementsInBuffer) {
        requestResponseTimeList.remove(0);
      }
      requestResponseTimeList.add(new RequestResponseTime(requestResponseTime));
    }
  }

  @TestOnly
  int getBufferSize() {
    return requestResponseTimeList.size();
  }
}
