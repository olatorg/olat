/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.monitoring;

import org.olat.core.CoreSpringFactory;
import org.olat.core.util.session.UserSessionManager;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 * @author Christian Guretzki
 * @since 6.1
 */
@Component
@ManagedResource(
    objectName = "ch.uzh.olat:type=basic,name=UserSessionStatistic",
    description = "Statistic of user session")
public class UserSessionStatistic implements UserSessionStatisticMBean {

  @ManagedAttribute(description = "Number of current authenticated users.")
  public int getAuthenicatedUsers() {
    UserSessionManager sessionManager = CoreSpringFactory.getImpl(UserSessionManager.class);
    return sessionManager.getUserSessionsCnt();
  }
}
