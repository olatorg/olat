/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.monitoring;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpFilter;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.CoreSpringFactory;

/**
 * @author Christian Guretzki
 * @since 6.1
 */
@Slf4j
public class RequestResponseStatisticFilter extends HttpFilter {

  private transient RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    super.init(filterConfig);
    log.info("RequestResponseStatisticFilter is activ");
  }

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    if (requestResponseTimeStatisticCollector == null) {
      requestResponseTimeStatisticCollector =
          CoreSpringFactory.getImpl(RequestResponseTimeStatisticCollector.class);
    }
    long requestStartTime = System.currentTimeMillis();
    filterChain.doFilter(servletRequest, servletResponse);
    long requestResponseTime = System.currentTimeMillis() - requestStartTime;
    requestResponseTimeStatisticCollector.addRequestResponseTime(requestResponseTime);
  }
}
