/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.ui;

import java.util.List;
import org.olat.core.commons.persistence.SortKey;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiSortableColumnDef;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableDataModel;

/**
 * @author Christian Guretzki
 * @since 5.0
 */
public class ModuleVersionTableModel extends DefaultFlexiTableDataModel<ModuleVersionRow>
    implements SortableFlexiTableDataModel<ModuleVersionRow> {

  public ModuleVersionTableModel(FlexiTableColumnModel columnModel) {
    super(columnModel);
  }

  @Override
  public Object getValueAt(int row, int column) {
    ModuleVersionRow participant = getObject(row);
    return getValueAt(participant, column);
  }

  @Override
  public void sort(SortKey sortKey) {
    if (sortKey != null) {
      List<ModuleVersionRow> rows = new ModuleVersionTableSorter(sortKey, this, null).sort();
      super.setObjects(rows);
    }
  }

  @Override
  public Object getValueAt(ModuleVersionRow moduleVersionRow, int column) {
    return switch (Columns.values()[column]) {
      case MODULE_NAME -> moduleVersionRow.getModuleName();
      case BUILD_TIME -> moduleVersionRow.getBuildTime();
      case VERSION -> moduleVersionRow.getVersion();
      case COMMIT_ID -> moduleVersionRow.getCommitId();
    };
  }

  public enum Columns implements FlexiSortableColumnDef {
    MODULE_NAME("table.header.module.name"),
    BUILD_TIME("table.header.build.time"),
    VERSION("table.header.version"),
    COMMIT_ID("table.header.commit.id");

    private final String i18nHeaderKey;

    Columns(String i18nHeaderKey) {
      this.i18nHeaderKey = i18nHeaderKey;
    }

    @Override
    public String i18nHeaderKey() {
      return i18nHeaderKey;
    }

    @Override
    public boolean sortable() {
      return true;
    }

    @Override
    public String sortKey() {
      return name();
    }
  }
}
