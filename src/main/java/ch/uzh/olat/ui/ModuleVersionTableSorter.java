/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.ui;

import java.util.Locale;
import org.olat.core.commons.persistence.SortKey;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableModelDelegate;

/**
 * @author Christian Guretzki
 * @since 5.0
 */
public class ModuleVersionTableSorter extends SortableFlexiTableModelDelegate<ModuleVersionRow> {

  public ModuleVersionTableSorter(
      SortKey orderBy, SortableFlexiTableDataModel<ModuleVersionRow> tableModel, Locale locale) {
    super(orderBy, tableModel, locale);
  }
}
