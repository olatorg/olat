/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.ui;

import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Christian Guretzki
 * @since 5.0
 */
@Getter
@RequiredArgsConstructor
public class ModuleVersionRow {

  private final String moduleName;
  private final String version;
  private final String buildTime;
  private final String commitId;
  private final String originUrl;

  public String getGitLabCommitUrl() {
    return "https://gitlab.com/olatorg/" + moduleName + "/-/commit/" + commitId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ModuleVersionRow that = (ModuleVersionRow) o;
    return Objects.equals(moduleName, that.moduleName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(moduleName);
  }
}
