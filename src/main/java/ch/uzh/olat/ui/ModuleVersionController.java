/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.ui;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.media.RedirectMediaResource;

/**
 * @author Christian Guretzki
 * @since 5.0
 */
@Slf4j
public class ModuleVersionController extends FormBasicController {
  private static final String ACTION_LINK_TO_COMMIT = "linkToCommit";
  private static final String GIT_GITLAB_OLATORG_ORIGIN_URL_PREFIX = "git@gitlab.com:olatorg/";
  private static final String GIT_GITLAB_OLATORG_INTERNAL_ORIGIN_URL_PREFIX =
      "git@gitlab.com:olatorg-internal/";
  private static final String GITLAB_CI_TOKEN_OLATORG_ORIGIN_URL_PREFIX =
      "https://gitlab-ci-token@gitlab.com/olatorg/";
  private static final String GITLAB_CI_TOKEN_OLATORG_INTERNAL_ORIGIN_URL_PREFIX =
      "https://gitlab-ci-token@gitlab.com/olatorg-internal/";
  private static final String GIT_ORIGIN_URL_PROJECT_POSTFIX = ".git";
  private static final String GIT_PROPERTIES_FILE_NAME = "git.properties";

  private ModuleVersionTableModel moduleVersionTableModel;

  public ModuleVersionController(UserRequest userRequest, WindowControl windowControl) {
    super(userRequest, windowControl, "module_version");
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller controller, UserRequest userRequest) {
    initModuleVersionTable(formLayout);
  }

  private void initModuleVersionTable(FormItemContainer formLayout) {
    FlexiTableColumnModel columnModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(ModuleVersionTableModel.Columns.MODULE_NAME));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(ModuleVersionTableModel.Columns.VERSION));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(ModuleVersionTableModel.Columns.BUILD_TIME));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            ModuleVersionTableModel.Columns.COMMIT_ID, ACTION_LINK_TO_COMMIT));

    moduleVersionTableModel = new ModuleVersionTableModel(columnModel);
    moduleVersionTableModel.setObjects(lookupModuleVersionInfos());
    FlexiTableElement moduleVersionTable =
        uifactory.addTableElement(
            getWindowControl(),
            "moduleVersionTable",
            moduleVersionTableModel,
            20,
            false,
            getTranslator(),
            formLayout);
    moduleVersionTable.setExportEnabled(true);
    moduleVersionTable.setCustomizeColumns(false);
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // Do nothing
  }

  private List<ModuleVersionRow> lookupModuleVersionInfos() {
    Set<ModuleVersionRow> moduleVersions = new HashSet<>();
    try {
      Enumeration<URL> gitPropertiesResources =
          Thread.currentThread().getContextClassLoader().getResources(GIT_PROPERTIES_FILE_NAME);
      while (gitPropertiesResources.hasMoreElements()) {
        URL url = gitPropertiesResources.nextElement();
        Optional<ModuleVersionRow> gitVersionInfoOpt = findOlatModuleVersionInfo(url.openStream());
        gitVersionInfoOpt.ifPresent(moduleVersions::add);
      }
    } catch (IOException e) {
      log.warn("Could not get GitVersionInfo from module, Exception={}", e.getMessage());
    }
    return moduleVersions.stream()
        .sorted(Comparator.comparing(ModuleVersionRow::getModuleName))
        .toList();
  }

  private Optional<ModuleVersionRow> findOlatModuleVersionInfo(InputStream is) {
    Properties prop = new Properties();
    try {
      prop.load(is);
      String originUrl = prop.getProperty("git.remote.origin.url", "-");
      if (originUrl.contains("olatorg")) {
        String moduleName = getModuleNameFromOriginUrl(originUrl);
        String buildVersion = prop.getProperty("git.build.version", "-");
        String buildTime = prop.getProperty("git.build.time", "-");
        String commitId = prop.getProperty("git.commit.id", "-");
        return Optional.of(
            new ModuleVersionRow(moduleName, buildVersion, buildTime, commitId, originUrl));
      }
    } catch (IOException e) {
      log.debug(
          "findOlatModuleVersionInfo: Could not load git.propertiers, Exception={}",
          e.getMessage());
    }
    return Optional.empty();
  }

  private String getModuleNameFromOriginUrl(String originUrl) {
    if (originUrl.startsWith(GIT_GITLAB_OLATORG_ORIGIN_URL_PREFIX)) {
      return originUrl
          .replace(GIT_GITLAB_OLATORG_ORIGIN_URL_PREFIX, "")
          .replace(GIT_ORIGIN_URL_PROJECT_POSTFIX, "");
    } else if (originUrl.startsWith(GIT_GITLAB_OLATORG_INTERNAL_ORIGIN_URL_PREFIX)) {
      return originUrl
          .replace(GIT_GITLAB_OLATORG_INTERNAL_ORIGIN_URL_PREFIX, "")
          .replace(GIT_ORIGIN_URL_PROJECT_POSTFIX, "");
    } else if (originUrl.startsWith(GITLAB_CI_TOKEN_OLATORG_ORIGIN_URL_PREFIX)) {
      return originUrl
          .replace(GITLAB_CI_TOKEN_OLATORG_ORIGIN_URL_PREFIX, "")
          .replace(GIT_ORIGIN_URL_PROJECT_POSTFIX, "");
    } else if (originUrl.startsWith(GITLAB_CI_TOKEN_OLATORG_INTERNAL_ORIGIN_URL_PREFIX)) {
      return originUrl
          .replace(GITLAB_CI_TOKEN_OLATORG_INTERNAL_ORIGIN_URL_PREFIX, "")
          .replace(GIT_ORIGIN_URL_PROJECT_POSTFIX, "");
    } else {
      log.warn("Unkown originUrl={} with unkown prefix", originUrl);
      return "-";
    }
  }

  @Override
  public void event(UserRequest userRequest, Component source, Event event) {
    log.debug("event Component source={}, event={}", source, event);
    if (event instanceof SelectionEvent selectionEvent
        && ACTION_LINK_TO_COMMIT.equals(event.getCommand())) {
      int index = selectionEvent.getIndex();
      ModuleVersionRow selected = moduleVersionTableModel.getObjects().get(index);
      userRequest
          .getDispatchResult()
          .setResultingMediaResource(new RedirectMediaResource(selected.getGitLabCommitUrl()));
    }
  }
}
