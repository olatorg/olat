/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.csp.ui;

import ch.uzh.olat.csp.service.AdditionalCspModule;
import ch.uzh.olat.csp.service.CspDefaultConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.commons.services.csp.CSPModule;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.TextElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Christian Guretzki
 * @since 6.0
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CspAdminController extends FormBasicController {

  private static final int MAX_INPUT_FIELD_LENGTH = 512;
  private static final int MAX_CSP_HEADER_SIZE = 6000;
  private static final String CSP_HEADER_MAX_SIZE_ERROR = "csp.header.max.size.error";
  @Autowired private CspDefaultConfiguration cspDefaultConfiguration;
  @Autowired private CSPModule cspModule;
  @Autowired private AdditionalCspModule additionalCspModule;
  private TextElement additionalDefaultSrcTextElement;
  private TextElement additionalScriptSrcTextElement;
  private TextElement additionalStyleSrcTextElement;
  private TextElement additionalImgSrcTextElement;
  private TextElement additionalFontSrcTextElement;
  private TextElement additionalConnectSrcTextElement;
  private TextElement additionalFrameSrcTextElement;
  private TextElement additionalMediaSrcTextElement;
  private TextElement additionalObjectSrcTextElement;
  private TextElement additionalPluginTypeTextElement;

  public CspAdminController(UserRequest userRequest, WindowControl windowControl) {
    super(userRequest, windowControl);
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller controller, UserRequest userRequest) {
    setFormTitle("csp.admin.title");
    if (cspModule.isContentSecurityPolicyEnabled()) {
      setFormDescription("csp.description.enabled");
      uifactory.addStaticTextElement(
          "defaultSrc",
          "default.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_DEFAULT_SRC,
              cspDefaultConfiguration.getUzhDefaultSrc()),
          formLayout);
      additionalDefaultSrcTextElement =
          uifactory.addTextElement(
              "additionalDefaultSrc",
              "additional.default.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalDefaultSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "scriptSrc",
          "default.script.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_SCRIPT_SRC,
              cspDefaultConfiguration.getUzhDefaultScriptSrc()),
          formLayout);
      additionalScriptSrcTextElement =
          uifactory.addTextElement(
              "additionalScriptSrc",
              "additional.script.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalScriptSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "styleSrc",
          "default.style.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_STYLE_SRC,
              cspDefaultConfiguration.getUzhDefaultStyleSrc()),
          formLayout);
      additionalStyleSrcTextElement =
          uifactory.addTextElement(
              "additionalStyleSrc",
              "additional.style.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalStyleSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "imgSrc",
          "default.img.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_IMG_SRC,
              cspDefaultConfiguration.getUzhDefaultImgSrc()),
          formLayout);
      additionalImgSrcTextElement =
          uifactory.addTextElement(
              "additionalImgSrc",
              "additional.img.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalImgSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "fontSrc",
          "default.font.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_FONT_SRC,
              cspDefaultConfiguration.getUzhDefaultFontSrc()),
          formLayout);
      additionalFontSrcTextElement =
          uifactory.addTextElement(
              "additionalFontSrc",
              "additional.font.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalFontSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "defaultConnectSrc",
          "default.connect.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_CONNECT_SRC,
              cspDefaultConfiguration.getUzhDefaultConnectSrc()),
          formLayout);
      additionalConnectSrcTextElement =
          uifactory.addTextElement(
              "additionalConnectSrc",
              "additional.connect.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalConnectSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "defaultFrameSrc",
          "default.frame.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_FRAME_SRC,
              cspDefaultConfiguration.getUzhDefaultFrameSrc()),
          formLayout);
      additionalFrameSrcTextElement =
          uifactory.addTextElement(
              "additionalFrameSrc",
              "additional.frame.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalFrameSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "defaultMediaSrc",
          "default.media.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_MEDIA_SRC,
              cspDefaultConfiguration.getUzhDefaultMediaSrc()),
          formLayout);
      additionalMediaSrcTextElement =
          uifactory.addTextElement(
              "additionalMediaSrc",
              "additional.media.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalMediaSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "defaultObjectSrc",
          "default.object.src.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_OBJECT_SRC,
              cspDefaultConfiguration.getUzhDefaultObjectSrc()),
          formLayout);
      additionalObjectSrcTextElement =
          uifactory.addTextElement(
              "additionalObjectSrc",
              "additional.object.src.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalObjectSrc(),
              formLayout);
      uifactory.addStaticTextElement(
          "defaultPluginType",
          "default.plugin.type.label",
          getCompleteDefaultDirective(
              CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_PLUGIN_TYPE_SRC,
              cspDefaultConfiguration.getUzhDefaultPluginType()),
          formLayout);
      additionalPluginTypeTextElement =
          uifactory.addTextElement(
              "additionalPluginType",
              "additional.plugin.type.label",
              MAX_INPUT_FIELD_LENGTH,
              additionalCspModule.getAdditionalPluginType(),
              formLayout);

      addCancelAndSubmitButton(formLayout, userRequest);
    } else {
      setFormDescription("csp.description.disabled");
    }
  }

  private String getCompleteDefaultDirective(String openOlatDefaultValue, String uzhDefaultValue) {
    StringBuilder sb = new StringBuilder();
    if (openOlatDefaultValue != null) {
      sb.append(openOlatDefaultValue);
    }
    if ((openOlatDefaultValue != null && !openOlatDefaultValue.endsWith(" "))
        && (uzhDefaultValue != null && !uzhDefaultValue.startsWith(" "))) {
      sb.append(" ");
    }
    if (uzhDefaultValue != null) {
      sb.append(uzhDefaultValue);
    }
    if (sb.toString().isBlank()) {
      return "-";
    } else {
      return sb.toString();
    }
  }

  private void addCancelAndSubmitButton(FormItemContainer formLayout, UserRequest userRequest) {
    FormLayoutContainer buttonLayout =
        FormLayoutContainer.createButtonLayout("button_layout", getTranslator());
    formLayout.add(buttonLayout);
    uifactory.addFormCancelButton("cancel", buttonLayout, userRequest, this.getWindowControl());
    uifactory.addFormSubmitButton("submit", buttonLayout);
  }

  @Override
  protected boolean validateFormLogic(UserRequest ureq) {
    boolean allOk = super.validateFormLogic(ureq);
    if (calculateCspHeaderSize() > MAX_CSP_HEADER_SIZE) {
      setErrorKeyAtChangedTextElement();
      allOk = false;
    }
    return allOk;
  }

  private void setErrorKeyAtChangedTextElement() {
    if (isModifedAndIncreased(
        additionalDefaultSrcTextElement.getValue(),
        additionalCspModule.getAdditionalDefaultSrc())) {
      additionalDefaultSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalScriptSrcTextElement.getValue(), additionalCspModule.getAdditionalScriptSrc())) {
      additionalScriptSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalStyleSrcTextElement.getValue(), additionalCspModule.getAdditionalStyleSrc())) {
      additionalStyleSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalImgSrcTextElement.getValue(), additionalCspModule.getAdditionalImgSrc())) {
      additionalImgSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalFontSrcTextElement.getValue(), additionalCspModule.getAdditionalFontSrc())) {
      additionalFontSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalConnectSrcTextElement.getValue(),
        additionalCspModule.getAdditionalConnectSrc())) {
      additionalConnectSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalFrameSrcTextElement.getValue(), additionalCspModule.getAdditionalFrameSrc())) {
      additionalFrameSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalMediaSrcTextElement.getValue(), additionalCspModule.getAdditionalMediaSrc())) {
      additionalMediaSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalObjectSrcTextElement.getValue(), additionalCspModule.getAdditionalObjectSrc())) {
      additionalObjectSrcTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else if (isModifedAndIncreased(
        additionalPluginTypeTextElement.getValue(),
        additionalCspModule.getAdditionalPluginType())) {
      additionalPluginTypeTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    } else {
      log.warn(
          "Found no changed TextElement => setErrorKey at additionalPluginTypeTextElement as fallback");
      additionalPluginTypeTextElement.setErrorKey(CSP_HEADER_MAX_SIZE_ERROR);
    }
  }

  private boolean isModifedAndIncreased(String textElementValue, String currentValue) {
    return (currentValue != null && textElementValue.length() > currentValue.length())
        || (currentValue == null && textElementValue.length() > 0);
  }

  private int calculateCspHeaderSize() {
    int headerSize =
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultSrc(), additionalDefaultSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultScriptSrc(), additionalScriptSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultStyleSrc(), additionalStyleSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultImgSrc(), additionalImgSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultFontSrc(), additionalFontSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultConnectSrc(), additionalConnectSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultFrameSrc(), additionalFrameSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultMediaSrc(), additionalMediaSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultObjectSrc(), additionalObjectSrcTextElement)
            .length();
    headerSize +=
        getDirectiveValue(
                cspDefaultConfiguration.getUzhDefaultPluginType(), additionalPluginTypeTextElement)
            .length();
    log.info("CSP Header size of UZH part:{}", headerSize);
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_DEFAULT_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_SCRIPT_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_STYLE_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_IMG_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_FONT_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_CONNECT_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_FRAME_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_MEDIA_SRC.length();
    headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_OBJECT_SRC.length();
    if (CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_PLUGIN_TYPE_SRC != null) {
      headerSize += CSPModule.DEFAULT_CONTENT_SECURITY_POLICY_PLUGIN_TYPE_SRC.length();
    }
    log.info("CSP Header size Total:{}", headerSize);
    return headerSize;
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    additionalCspModule.setAdditionalDefaultSrc(additionalDefaultSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyDefaultSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultSrc(), additionalDefaultSrcTextElement));
    additionalCspModule.setAdditionalScriptSrc(additionalScriptSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyScriptSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultScriptSrc(), additionalScriptSrcTextElement));
    additionalCspModule.setAdditionalStyleSrc(additionalStyleSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyStyleSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultStyleSrc(), additionalStyleSrcTextElement));
    additionalCspModule.setAdditionalImgSrc(additionalImgSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyImgSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultImgSrc(), additionalImgSrcTextElement));
    additionalCspModule.setAdditionalFontSrc(additionalFontSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyFontSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultFontSrc(), additionalFontSrcTextElement));
    additionalCspModule.setAdditionalConnectSrc(additionalConnectSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyConnectSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultConnectSrc(), additionalConnectSrcTextElement));
    additionalCspModule.setAdditionalFrameSrc(additionalFrameSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyFrameSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultFrameSrc(), additionalFrameSrcTextElement));
    additionalCspModule.setAdditionalMediaSrc(additionalMediaSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyMediaSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultMediaSrc(), additionalMediaSrcTextElement));
    additionalCspModule.setAdditionalObjectSrc(additionalObjectSrcTextElement.getValue());
    cspModule.setContentSecurityPolicyObjectSrc(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultObjectSrc(), additionalObjectSrcTextElement));
    additionalCspModule.setAdditionalPluginType(additionalPluginTypeTextElement.getValue());
    cspModule.setContentSecurityPolicyPluginType(
        getDirectiveValue(
            cspDefaultConfiguration.getUzhDefaultPluginType(), additionalPluginTypeTextElement));
  }

  private String getDirectiveValue(String defaultValue, TextElement additionalTextElement) {
    StringBuilder sb = new StringBuilder();
    if (defaultValue != null) {
      sb.append(defaultValue);
    }
    if (!additionalTextElement.getValue().isBlank()
        && (defaultValue != null && !defaultValue.endsWith(" "))
        && !additionalTextElement.getValue().startsWith(" ")) {
      sb.append(" ");
    }
    if (!additionalTextElement.getValue().isBlank()) {
      sb.append(additionalTextElement.getValue());
    }
    return sb.toString();
  }
}
