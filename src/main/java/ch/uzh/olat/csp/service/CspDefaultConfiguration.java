/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.csp.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Christian Guretzki
 * @since 6.0
 */
@Getter
@Configuration
@PropertySource("classpath:ch/uzh/olat/csp/ui/_resources/csp_default.properties")
public class CspDefaultConfiguration {

  @Value("${uzh.default.src}")
  private String uzhDefaultSrc;

  @Value("${uzh.default.script.src}")
  private String uzhDefaultScriptSrc;

  @Value("${uzh.default.style.src}")
  private String uzhDefaultStyleSrc;

  @Value("${uzh.default.img.src}")
  private String uzhDefaultImgSrc;

  @Value("${uzh.default.font.src}")
  private String uzhDefaultFontSrc;

  @Value("${uzh.default.connect.src}")
  private String uzhDefaultConnectSrc;

  @Value("${uzh.default.frame.src}")
  private String uzhDefaultFrameSrc;

  @Value("${uzh.default.media.src}")
  private String uzhDefaultMediaSrc;

  @Value("${uzh.default.object.src}")
  private String uzhDefaultObjectSrc;

  @Value("${uzh.default.plugin.type}")
  private String uzhDefaultPluginType;
}
