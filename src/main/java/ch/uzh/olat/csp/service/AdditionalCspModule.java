/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.csp.service;

import lombok.extern.slf4j.Slf4j;
import org.olat.core.configuration.AbstractSpringModule;
import org.olat.core.util.StringHelper;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Christian Guretzki
 * @since 6.0
 */
@Slf4j
@Service
public class AdditionalCspModule extends AbstractSpringModule {

  private static final String ADDITIONAL_DEFAULT_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.defaultSrc";
  private static final String ADDITIONAL_SCRIPT_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.scriptSrc";
  private static final String ADDITIONAL_STYLE_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.styleSrc";
  private static final String ADDITIONAL_IMG_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.imgSrc";
  private static final String ADDITIONAL_FONT_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.fontSrc";
  private static final String ADDITIONAL_CONNECT_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.connectSrc";
  private static final String ADDITIONAL_FRAME_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.frameSrc";
  private static final String ADDITIONAL_MEDIA_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.mediaSrc";
  private static final String ADDITIONAL_OBJECT_SRC_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.objectSrc";
  private static final String ADDITIONAL_PLUGIN_TYPE_PROPERTY_NAME =
      "ch.uzh.olat.csp.additional.pluginType";

  private String additionalDefaultSrc;
  private String additionalScriptSrc;
  private String additionalStyleSrc;
  private String additionalImgSrc;
  private String additionalFontSrc;
  private String additionalConnectSrc;
  private String additionalFrameSrc;
  private String additionalMediaSrc;
  private String additionalObjectSrc;
  private String additionalPluginType;

  @Autowired
  public AdditionalCspModule(CoordinatorManager coordinatorManager) {
    super(coordinatorManager);
  }

  public void init() {
    this.updateProperties();
  }

  protected void initFromChangedProperties() {
    this.updateProperties();
  }

  private void updateProperties() {
    String defaultSrcValue =
        this.getStringPropertyValue(ADDITIONAL_DEFAULT_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(defaultSrcValue)) {
      this.additionalDefaultSrc = defaultSrcValue;
    }
    String scriptSrcValue = this.getStringPropertyValue(ADDITIONAL_SCRIPT_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(scriptSrcValue)) {
      this.additionalScriptSrc = scriptSrcValue;
    }
    String styleSrcValue = this.getStringPropertyValue(ADDITIONAL_STYLE_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(styleSrcValue)) {
      this.additionalStyleSrc = styleSrcValue;
    }
    String imgSrcValue = this.getStringPropertyValue(ADDITIONAL_IMG_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(imgSrcValue)) {
      this.additionalImgSrc = imgSrcValue;
    }
    String fontSrcValue = this.getStringPropertyValue(ADDITIONAL_FONT_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(fontSrcValue)) {
      this.additionalFontSrc = fontSrcValue;
    }
    String connectSrcValue =
        this.getStringPropertyValue(ADDITIONAL_CONNECT_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(connectSrcValue)) {
      this.additionalConnectSrc = connectSrcValue;
    }
    String frameSrcValue = this.getStringPropertyValue(ADDITIONAL_FRAME_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(frameSrcValue)) {
      this.additionalFrameSrc = frameSrcValue;
    }
    String mediaSrcValue = this.getStringPropertyValue(ADDITIONAL_MEDIA_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(mediaSrcValue)) {
      this.additionalMediaSrc = mediaSrcValue;
    }
    String objectSrcValue = this.getStringPropertyValue(ADDITIONAL_OBJECT_SRC_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(objectSrcValue)) {
      this.additionalObjectSrc = objectSrcValue;
    }
    String pluginTypeValue =
        this.getStringPropertyValue(ADDITIONAL_PLUGIN_TYPE_PROPERTY_NAME, true);
    if (StringHelper.containsNonWhitespace(pluginTypeValue)) {
      this.additionalPluginType = pluginTypeValue;
    }
  }

  public String getAdditionalDefaultSrc() {
    return this.additionalDefaultSrc;
  }

  public void setAdditionalDefaultSrc(String additionalValues) {
    this.additionalDefaultSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_DEFAULT_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalScriptSrc() {
    return this.additionalScriptSrc;
  }

  public void setAdditionalScriptSrc(String additionalValues) {
    this.additionalScriptSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_SCRIPT_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalConnectSrc() {
    return this.additionalConnectSrc;
  }

  public void setAdditionalConnectSrc(String additionalValues) {
    this.additionalConnectSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_CONNECT_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalStyleSrc() {
    return this.additionalStyleSrc;
  }

  public void setAdditionalStyleSrc(String additionalValues) {
    this.additionalStyleSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_STYLE_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalImgSrc() {
    return this.additionalImgSrc;
  }

  public void setAdditionalImgSrc(String additionalValues) {
    this.additionalImgSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_IMG_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalFontSrc() {
    return this.additionalFontSrc;
  }

  public void setAdditionalFontSrc(String additionalValues) {
    this.additionalFontSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_FONT_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalFrameSrc() {
    return this.additionalFrameSrc;
  }

  public void setAdditionalFrameSrc(String additionalValues) {
    this.additionalFrameSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_FRAME_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalMediaSrc() {
    return this.additionalMediaSrc;
  }

  public void setAdditionalMediaSrc(String additionalValues) {
    this.additionalMediaSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_MEDIA_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalObjectSrc() {
    return this.additionalObjectSrc;
  }

  public void setAdditionalObjectSrc(String additionalValues) {
    this.additionalObjectSrc = additionalValues;
    this.setStringProperty(ADDITIONAL_OBJECT_SRC_PROPERTY_NAME, additionalValues, true);
  }

  public String getAdditionalPluginType() {
    return this.additionalPluginType;
  }

  public void setAdditionalPluginType(String additionalValues) {
    this.additionalPluginType = additionalValues;
    this.setStringProperty(ADDITIONAL_PLUGIN_TYPE_PROPERTY_NAME, additionalValues, true);
  }
}
