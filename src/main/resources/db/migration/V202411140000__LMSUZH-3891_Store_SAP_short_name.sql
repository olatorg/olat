/*
 * Fill column o_user.u_generictextproperty with sap.student.short_name or sap.lecturer.short_name
 * of all mapped students or lecturers.
 */
DO
$$
    BEGIN

        IF
            EXISTS(SELECT schema_name
                   FROM information_schema.schemata
                   WHERE schema_name = 'ck')
        THEN
            UPDATE o_user
            SET u_generictextproperty =
                    (SELECT s.short_name
                     FROM ck.student_mapping sm
                              JOIN sap.student s ON sm.fk_student = s.id
                     WHERE sm.fk_mapped_identity = fk_identity
                       AND s.short_name IS NOT NULL
                     LIMIT 1)
            WHERE u_generictextproperty IS NULL;
            UPDATE o_user
            SET u_generictextproperty =
                    (SELECT l.short_name
                     FROM ck.lecturer_mapping lm
                              JOIN sap.lecturer l ON lm.fk_lecturer = l.employee_number
                     WHERE lm.fk_mapped_identity = fk_identity
                       AND l.short_name IS NOT NULL
                     LIMIT 1)
            WHERE u_generictextproperty IS NULL;
        END IF;
    END
$$;
