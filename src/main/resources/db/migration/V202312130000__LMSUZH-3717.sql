-- noinspection SqlWithoutWhereForFile

-- u_institutional_employee_number -> u_employeenumber
DO $$
    BEGIN
        IF EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_name = 'o_user' AND column_name = 'u_institutional_employee_number')
        THEN
            UPDATE o_user SET u_employeenumber = u_institutional_employee_number;
            ALTER TABLE o_user DROP COLUMN u_institutional_employee_number;
        END IF;
    END $$;

-- u_institutional_matriculation_number -> u_institutionaluseridentifier
DO $$
    BEGIN
        IF EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_name = 'o_user' AND column_name = 'u_institutional_matriculation_number')
        THEN
            UPDATE o_user SET u_institutionaluseridentifier = u_institutional_matriculation_number;
            ALTER TABLE o_user DROP COLUMN u_institutional_matriculation_number;
        END IF;
    END $$;