-- Add unique constraint to o_bs_group_member.
DO
$$
    BEGIN
        IF
            NOT EXISTS(SELECT *
                       FROM information_schema.table_constraints
                       WHERE table_name = 'o_bs_group_member'
                         AND constraint_type = 'UNIQUE'
                         AND constraint_name =
                             'o_bs_group_member_g_role_fk_group_id_fk_identity_id_key')
        THEN
            ALTER TABLE o_bs_group_member
                ADD UNIQUE (g_role, fk_group_id, fk_identity_id);
        END IF;
    END
$$;

-- Add unique constraint to o_re_to_group.
DO
$$
    BEGIN
        IF
            NOT EXISTS(SELECT *
                       FROM information_schema.table_constraints
                       WHERE table_name = 'o_re_to_group'
                         AND constraint_type = 'UNIQUE'
                         AND constraint_name =
                             'o_re_to_group_r_defgroup_fk_group_id_fk_entry_id_key')
        THEN
            ALTER TABLE o_re_to_group
                ADD UNIQUE (r_defgroup, fk_group_id, fk_entry_id);
        END IF;
    END
$$;
