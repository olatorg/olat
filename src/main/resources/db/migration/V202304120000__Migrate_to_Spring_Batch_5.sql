-- noinspection SqlWithoutWhereForFile

-- Migrate to Spring Batch 5.0

-- 1. Migrate batch_job_execution table
DO $$
    BEGIN
        IF EXISTS(SELECT *
                      FROM information_schema.columns
                      WHERE table_name = 'batch_job_execution' AND column_name = 'job_configuration_location')
        THEN
            ALTER TABLE batch_job_execution DROP COLUMN job_configuration_location;
        END IF;
    END $$;


-- 2. Migrate batch_step_execution table (keep column order!)
DO $$
    BEGIN
        IF (EXISTS(SELECT *
                      FROM information_schema.columns
                      WHERE table_name = 'batch_step_execution')
            AND NOT EXISTS(SELECT *
                      FROM information_schema.columns
                      WHERE table_name = 'batch_step_execution' AND column_name = 'create_time'))
        THEN
            -- Rename existing columns to <column_name>_old to keep original column order
            ALTER TABLE batch_step_execution RENAME COLUMN start_time TO start_time_old;
            ALTER TABLE batch_step_execution RENAME COLUMN end_time TO end_time_old;
            ALTER TABLE batch_step_execution RENAME COLUMN status TO status_old;
            ALTER TABLE batch_step_execution RENAME COLUMN commit_count TO commit_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN read_count TO read_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN filter_count TO filter_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN write_count TO write_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN read_skip_count TO read_skip_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN write_skip_count TO write_skip_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN process_skip_count TO process_skip_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN rollback_count TO rollback_count_old;
            ALTER TABLE batch_step_execution RENAME COLUMN exit_code TO exit_code_old;
            ALTER TABLE batch_step_execution RENAME COLUMN exit_message TO exit_message_old;
            ALTER TABLE batch_step_execution RENAME COLUMN last_updated TO last_updated_old;

            -- Insert new columns
            ALTER TABLE batch_step_execution ADD COLUMN create_time TIMESTAMP WITHOUT TIME ZONE;
            ALTER TABLE batch_step_execution ADD COLUMN start_time TIMESTAMP WITHOUT TIME ZONE;
            ALTER TABLE batch_step_execution ADD COLUMN end_time TIMESTAMP WITHOUT TIME ZONE;
            ALTER TABLE batch_step_execution ADD COLUMN status VARCHAR(10);
            ALTER TABLE batch_step_execution ADD COLUMN commit_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN read_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN filter_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN write_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN read_skip_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN write_skip_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN process_skip_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN rollback_count BIGINT;
            ALTER TABLE batch_step_execution ADD COLUMN exit_code VARCHAR(2500);
            ALTER TABLE batch_step_execution ADD COLUMN exit_message VARCHAR(2500);
            ALTER TABLE batch_step_execution ADD COLUMN last_updated TIMESTAMP WITHOUT TIME ZONE;

            -- Migrate data
            UPDATE batch_step_execution
            SET create_time = start_time_old,
                start_time = start_time_old,
                end_time = end_time_old,
                status = status_old,
                commit_count = commit_count_old,
                read_count = read_count_old,
                filter_count = filter_count_old,
                write_count = write_count_old,
                read_skip_count = read_skip_count_old,
                write_skip_count = write_skip_count_old,
                process_skip_count = process_skip_count_old,
                rollback_count = rollback_count_old,
                exit_code = exit_code_old,
                exit_message = exit_message_old,
                last_updated = last_updated_old;

            -- Add not null constraints
            ALTER TABLE batch_step_execution ALTER COLUMN create_time SET NOT NULL;

            -- Drop not null contraint
            ALTER TABLE batch_step_execution ALTER COLUMN start_time DROP NOT NULL;

            -- Drop old columns
            ALTER TABLE batch_step_execution DROP COLUMN start_time_old;
            ALTER TABLE batch_step_execution DROP COLUMN end_time_old;
            ALTER TABLE batch_step_execution DROP COLUMN status_old;
            ALTER TABLE batch_step_execution DROP COLUMN commit_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN read_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN filter_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN write_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN read_skip_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN write_skip_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN process_skip_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN rollback_count_old;
            ALTER TABLE batch_step_execution DROP COLUMN exit_code_old;
            ALTER TABLE batch_step_execution DROP COLUMN exit_message_old;
            ALTER TABLE batch_step_execution DROP COLUMN last_updated_old;
        END IF;
    END $$;


-- 3. Migrate batch_job_execution_params table (keep column order!)
DO $$
    BEGIN
        IF EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_name = 'batch_job_execution_params' AND column_name = 'type_cd')
        THEN
            -- Rename 'identifying' column to 'identifying_old' to keep original column order
            ALTER TABLE batch_job_execution_params RENAME COLUMN identifying TO identifying_old;

            -- Rename 'key_name' column and update type
            ALTER TABLE batch_job_execution_params RENAME COLUMN key_name TO parameter_name;
            ALTER TABLE batch_job_execution_params ALTER COLUMN parameter_name TYPE VARCHAR(100);

            -- Add new columns (note: column 'identifying' is created to keep original column order)
            ALTER TABLE batch_job_execution_params ADD COLUMN parameter_type VARCHAR(100);
            ALTER TABLE batch_job_execution_params ADD COLUMN parameter_value VARCHAR(2500);
            ALTER TABLE batch_job_execution_params ADD COLUMN identifying CHAR(1);

            -- Copy data from column 'identifying_old' to new column 'identifying'
            UPDATE batch_job_execution_params SET identifying = identifying_old;

            -- Migrate data (OLAT only contains data with type_cd = 'LONG' or type_cd = 'DATE')
            UPDATE batch_job_execution_params
                SET parameter_type = 'java.lang.Long', parameter_value = long_val
                WHERE type_cd = 'LONG';
            UPDATE batch_job_execution_params
                SET parameter_type = 'java.time.LocalDateTime',
                    parameter_value = REPLACE(TO_CHAR(date_val, 'YYYY-MM-DD HH24:MI:SS.MS'), ' ', 'T')
                WHERE type_cd = 'DATE';

            -- Add not null constraints
            ALTER TABLE batch_job_execution_params ALTER COLUMN parameter_type SET NOT NULL;
            ALTER TABLE batch_job_execution_params ALTER COLUMN identifying SET NOT NULL;

            -- Drop old columns
            ALTER TABLE batch_job_execution_params DROP COLUMN type_cd;
            ALTER TABLE batch_job_execution_params DROP COLUMN string_val;
            ALTER TABLE batch_job_execution_params DROP COLUMN date_val;
            ALTER TABLE batch_job_execution_params DROP COLUMN long_val;
            ALTER TABLE batch_job_execution_params DROP COLUMN double_val;
            ALTER TABLE batch_job_execution_params DROP COLUMN identifying_old;
        END IF;
    END $$;
