/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.app;

import ch.uzh.olat.app.ApplicationTest.Initializer;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest(
    properties = {
      "userdata.dir=${java.io.tmpdir}/${random.int}",
      "olat.kaltura.tenants[0].name=empty",
      "olat.kaltura.default-tenant=empty",
      "olat.switchcast.tenants.empty.url=https://example.org"
    },
    webEnvironment = WebEnvironment.RANDOM_PORT)
@Testcontainers
@ContextConfiguration(initializers = {Initializer.class})
@SuppressWarnings("SpringBootApplicationProperties")
class ApplicationTest {

  @Container
  @SuppressWarnings("resource")
  private static final PostgreSQLContainer<?> POSTGRESQL =
      new PostgreSQLContainer<>(DockerImageName.parse("postgres:12-alpine"))
          .withDatabaseName("olat")
          .withUsername("olat")
          .withPassword("olat");

  @Test
  @SuppressWarnings("java:S2699")
  void contextLoads() {}

  static class Initializer
      implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @SneakyThrows
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      TestPropertyValues.of(
              "db.host=" + POSTGRESQL.getHost(), "db.host.port=" + POSTGRESQL.getFirstMappedPort())
          .applyTo(applicationContext.getEnvironment());
    }
  }
}
