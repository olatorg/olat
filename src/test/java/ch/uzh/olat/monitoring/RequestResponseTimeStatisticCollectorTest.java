/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.monitoring;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@SuppressWarnings("java:S2925")
@Slf4j
class RequestResponseTimeStatisticCollectorTest {

  private static final int MIN_REQUEST_RESPONSE_TIME_10 = 10;
  private static final int MIN_REQUEST_RESPONSE_TIME_100 = 100;
  private static final int MAX_REQUEST_RESPONSE_TIME_400 = 400;
  private static final int MAX_REQUEST_RESPONSE_TIME_10000 = 10000;

  @Test
  void addRequest() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        new RequestResponseTimeStatisticCollector(10, 120000);
    int numberOfCalls = 1000;
    long sleepTime = 10;
    int numberOfThread = 100;

    for (int i = 0; i < numberOfThread; i++) {
      Thread thread =
          new AddRequestThread(
              "TestThread" + i, requestResponseTimeStatisticCollector, numberOfCalls, sleepTime);
      thread.start();
    }
    Thread.sleep(1000);
    Thread getterThread =
        new GetterThread("GetterThread", requestResponseTimeStatisticCollector, 300, sleepTime);
    getterThread.start();
    Thread.sleep(6000);
    log.info(
        "RequestResponseTimeStatisticCollectorTest: mean={}, max={}",
        requestResponseTimeStatisticCollector.getMeanRequestResponseTime(),
        requestResponseTimeStatisticCollector.getMaxRequestResponseTime());
    assertEquals(999, requestResponseTimeStatisticCollector.getMaxRequestResponseTime());
    assertEquals(995.0, requestResponseTimeStatisticCollector.getMeanRequestResponseTime(), 5.0);
    assertTrue(requestResponseTimeStatisticCollector.getBufferSize() < 1000);
  }

  @Test
  void addRequest_PERFORMANCE() {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        new RequestResponseTimeStatisticCollector(10, 120000);
    long start = System.nanoTime();
    for (int i = MIN_REQUEST_RESPONSE_TIME_100; i < 200; i++) {
      requestResponseTimeStatisticCollector.addRequestResponseTime(i);
    }
    long end = System.nanoTime();
    log.info("TEST addRequest_PERFORMANCE: addRequestResponseTime duration=" + (end - start));

    start = System.nanoTime();
    for (int i = 0; i < 200; i++) {
      requestResponseTimeStatisticCollector.getMeanRequestResponseTime();
    }
    end = System.nanoTime();
    log.info("TEST addRequest_PERFORMANCE getMeanRequestResponseTime duration=" + (end - start));
    // ExpectedValue: maxElementsInBuffer=10 => 190+191+..+198+199 = 1945/10 => 194.5
    assertEquals(194.5, requestResponseTimeStatisticCollector.getMeanRequestResponseTime());
  }

  @Test
  void getMaxRequestResponseTime_WITH_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(true);
    assertEquals(
        MAX_REQUEST_RESPONSE_TIME_400,
        requestResponseTimeStatisticCollector.getMaxRequestResponseTime());
  }

  @Test
  void getMinRequestResponseTime_WITH_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(true);
    assertEquals(
        MIN_REQUEST_RESPONSE_TIME_100,
        requestResponseTimeStatisticCollector.getMinRequestResponseTime());
  }

  @Test
  void getMeanRequestResponseTime_WITH_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(true);
    // ExpectedValue: maxElementsInBuffer=10 => 200 + 100 + 400 + 300 = 1000/4 => 250
    assertEquals(250, requestResponseTimeStatisticCollector.getMeanRequestResponseTime());
  }

  @Test
  void getMaxRequestResponseTime_WITHOUT_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(false);
    assertEquals(
        MAX_REQUEST_RESPONSE_TIME_10000,
        requestResponseTimeStatisticCollector.getMaxRequestResponseTime());
  }

  @Test
  void getMinRequestResponseTime_WITHOUT_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(false);
    assertEquals(
        MIN_REQUEST_RESPONSE_TIME_10,
        requestResponseTimeStatisticCollector.getMinRequestResponseTime());
  }

  @Test
  void getMeanRequestResponseTime_WITHOUT_EXPIRED_ELEMENTS() throws InterruptedException {
    RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector =
        createRequestResponseTimeStatisticCollector(false);
    // ExpectedValue: maxElementsInBuffer=10 => 10000 + 10 + 200 + 100 + 400 + 300 = 11010/6 => 1835
    assertEquals(1835, requestResponseTimeStatisticCollector.getMeanRequestResponseTime());
  }

  private RequestResponseTimeStatisticCollector createRequestResponseTimeStatisticCollector(
      boolean hasExpiredElements) throws InterruptedException {
    RequestResponseTimeStatisticCollector createdRequestResponseTimeStatisticCollector =
        new RequestResponseTimeStatisticCollector(10, 100);
    // Expired Elements
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(
        MAX_REQUEST_RESPONSE_TIME_10000);
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(
        MIN_REQUEST_RESPONSE_TIME_10);
    if (hasExpiredElements) {
      sleepToHaveExpiredElements();
    }
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(200);
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(
        MIN_REQUEST_RESPONSE_TIME_100);
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(
        MAX_REQUEST_RESPONSE_TIME_400);
    createdRequestResponseTimeStatisticCollector.addRequestResponseTime(300);
    return createdRequestResponseTimeStatisticCollector;
  }

  private void sleepToHaveExpiredElements() throws InterruptedException {
    Thread.sleep(200);
  }
}

@SuppressWarnings("java:S2925")
@Slf4j
class AddRequestThread extends Thread {

  private final long sleepBound;
  private final int numberOfCalls;
  private final String threadName;
  private final RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector;

  public AddRequestThread(
      String threadName,
      RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector,
      int numberOfCalls,
      long sleepBound) {
    this.threadName = threadName;
    this.requestResponseTimeStatisticCollector = requestResponseTimeStatisticCollector;
    this.numberOfCalls = numberOfCalls;
    this.sleepBound = sleepBound;
  }

  @Override
  public void run() {
    try {
      log.info("Thread:{} started", threadName);
      long startTime = System.currentTimeMillis();
      Random randomSleepGenerator = new Random();
      for (int i = 0; i < numberOfCalls; i++) {
        requestResponseTimeStatisticCollector.addRequestResponseTime(i);
        Thread.sleep(randomSleepGenerator.nextLong(sleepBound));
      }
      log.info(
          "Thread:{} finished after {}sec", threadName, System.currentTimeMillis() - startTime);

    } catch (InterruptedException e) {
      log.warn("Thread:{} Exception:{}", threadName, e);
    }
  }
}

@SuppressWarnings("java:S2925")
@Slf4j
class GetterThread extends Thread {

  private final long sleepBound;
  private final int numberOfCalls;
  private final String threadName;
  private final RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector;

  public GetterThread(
      String threadName,
      RequestResponseTimeStatisticCollector requestResponseTimeStatisticCollector,
      int numberOfCalls,
      long sleepBound) {
    this.threadName = threadName;
    this.requestResponseTimeStatisticCollector = requestResponseTimeStatisticCollector;
    this.numberOfCalls = numberOfCalls;
    this.sleepBound = sleepBound;
  }

  @Override
  public void run() {
    try {
      log.info("Thread:{} started", threadName);
      long startTime = System.currentTimeMillis();
      Random randomSleepGenerator = new Random();
      for (int i = 0; i < numberOfCalls; i++) {
        requestResponseTimeStatisticCollector.getMinRequestResponseTime();
        Thread.sleep(randomSleepGenerator.nextLong(sleepBound));
        requestResponseTimeStatisticCollector.getMaxRequestResponseTime();
        Thread.sleep(randomSleepGenerator.nextLong(sleepBound));
        requestResponseTimeStatisticCollector.getMeanRequestResponseTime();
        Thread.sleep(randomSleepGenerator.nextLong(sleepBound));
      }
      log.info(
          "Thread:{} finished after {}sec", threadName, System.currentTimeMillis() - startTime);

    } catch (InterruptedException e) {
      log.warn("Thread:{} Exception:{}", threadName, e);
    }
  }
}
