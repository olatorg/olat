# OLAT

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat-18.2--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/OpenOLAT)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.2.11-6bb536)](https://docs.spring.io/spring-boot/docs/3.2.11/reference/html/)

[OLAT](https://www.olat.uzh.ch/) is the
strategic [learning management system (LMS)](https://en.wikipedia.org/wiki/Learning_management_system)
at the [University of Zurich (UZH)](https://www.uzh.ch/). It is used by all faculties and can be
used in a variety of ways to provide content or to support the organization of learning processes.
Since OLAT has been developed at the University of Zurich and is constantly being further developed,
the needs and wishes of the users flow directly into the further development.

## Usage

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat.git
   cd olat
   ```

2. Build project.

   ```shell
   ./mvnw verify
   ```

3. Create a local PostgreSQL database (e.g. with [Docker](https://docs.docker.com/get-started/)).

   ```shell
   docker run --name olat-db --env POSTGRES_USER=olat --env POSTGRES_PASSWORD=olat -p 5432:5432 -v olat-db_data:/var/lib/postgresql/data --detach postgres:12-alpine
   ```

4. It is possible to provide custom properties, without need to add them to the Git repository (e.g.
   credentials).

   ```shell
   vim .local.yml
   ```

   The `.local.yml` file can be configured the same way as an `application.yml` file.
   It is only supported with the `development` profile (activated by default).

5. Start application

   ```shell
   java -jar olat-app/target/olat-app*.jar
   ```

6. Open browser and navigate to [http://localhost:8080/](http://localhost:8080/).

## Docker

OLAT can be used with [Docker](https://www.docker.com/get-started/)
and [Docker Compose](https://docs.docker.com/compose/).

Use the following commands to start OLAT with the provided `docker-compose.yml` file.

1. Authenticate with
   the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/).

   ```shell
   docker login registry.gitlab.com -u <username> -p <token>
   ```

2. Create a `.env` file to pass environment variables.

   ```shell
   cat > .env<< EOF
   TAG=<tag>
   EOF
   ```

3. Start Docker containers.

   ```shell
   docker-compose up --detach
   ```

4. Stop and remove containers and volumes

   ```shell
   docker-compose down --volumes
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
